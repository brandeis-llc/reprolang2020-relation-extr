from typing import List

from util import vocab
from util.io_models import Relation
from util.vocab import ENTSWRD, ENTEWRD, ENTEPOS, ENTSPOS


class TaggedToken(object):
    def __init__(self, word, tag):
        self.word = word
        self.tag  = tag
        # I don't like these variable names (left position, right position) as they are confusing with POS tags
        self.rpos = 0
        self.lpos = 0

    def __eq__(self, other):
        return self.word == other.word and self.tag == other.tag

    def __repr__(self):
        return f'"{self.word}/{self.tag}:{self.lpos}/{self.rpos}"'


class RelationInstance(Relation):
    befr: List[TaggedToken]
    ent1: List[TaggedToken]
    ent2: List[TaggedToken]
    btwn: List[TaggedToken]
    aftr: List[TaggedToken]

    def __init__(self, arg1, arg2, label, backward=False):
        super().__init__(arg1, arg2, label, backward)
        self.reset()

    def reset(self):
        tokens = self.sent_to_taggedtokens(self.sent)
        self.befr = tokens[:self.arg1.start]
        self.ent1 = tokens[self.arg1.start:self.arg1.end]
        self.btwn = tokens[self.arg1.end:self.arg2.start]
        self.ent2 = tokens[self.arg2.start:self.arg2.end]
        self.aftr = tokens[self.arg2.end:]

    def short_enough(self, threshold):
        return len(self.btwn) <= threshold

    @classmethod
    def sent_to_taggedtokens(cls, sent):
        return [TaggedToken(w, t) for w, t in zip(sent.tokens, sent.pos)]

    def insert_boundaries(self):
        e1stok = TaggedToken(ENTSWRD, ENTSPOS)
        e1etok = TaggedToken(ENTEWRD, ENTEPOS)
        e2stok = TaggedToken(ENTSWRD, ENTSPOS)
        e2etok = TaggedToken(ENTEWRD, ENTEPOS)
        self.ent1 = [e1stok] + self.ent1 + [e1etok]
        self.ent2 = [e2stok] + self.ent2 + [e2etok]
        self.parts = [self.befr, self.ent1, self.btwn, self.ent2, self.aftr]

    def assign_positions(self):
        e1len = len(self.ent1)
        e2len = len(self.ent2)
        btlen = len(self.btwn)
        for lpos, token in enumerate(self.btwn, 1):
            token.lpos = lpos
            token.rpos = btlen - lpos + 1
        for e2lpos, token in enumerate(self.ent2, btlen+1):
            token.lpos = e2lpos
        for e1rpos, token in enumerate(reversed(self.ent1), btlen+1):
            token.rpos = e1rpos
        for aflpos, token in enumerate(self.aftr, btlen+e2len+1):
            token.lpos = aflpos
        for bfrpos, token in enumerate(reversed(self.aftr), btlen+e1len+1):
            token.rpos = bfrpos

    def reverse(self):
        tmp = self.ent2
        self.ent2 = self.ent1
        self.ent1 = tmp
        self.ent1.reverse()
        self.ent2.reverse()
        self.btwn.reverse()
        tmp = self.befr
        self.befr = self.aftr
        self.aftr = tmp
        self.befr.reverse()
        self.aftr.reverse()

    def num_to_wildcard(self):
        import util.vocab
        for part in self.parts:
            for token in part:
                if token.tag == "CD":
                    token.word = util.vocab.NUMBER_WILDCARD


    def purge_parentheses(self):
        # Because CoreNLP tokenizer/tagger performs normalization on words,
        # this dict needs to cover normalized forms as well.
        # See https://nlp.stanford.edu/nlp/javadoc/javanlp-3.9.1/edu/stanford/nlp/process/PTBTokenizer.html
        # for normalization rules.
        bracket_pairs = {
            '(': ')',
            '[': ']',
            '-LRB-': '-RRB-',
            '-LSB-': '-RSB-',
        }
        brackets = list(bracket_pairs.keys()) + list(bracket_pairs.values())

        def purge_parenthsis(tokens: List[TaggedToken]):
            words = [token.word for token in tokens]
            for opening in bracket_pairs:
                try:
                    open_idx = words.index(opening)
                    closing = bracket_pairs[opening]
                    close_idx = words[open_idx:].index(closing)
                    tokens = tokens[:open_idx] + tokens[open_idx + close_idx + 1:]
                except ValueError:
                    # no parentheses found, do nothing.
                    pass
            return tokens

        def purge_brackets(tokens: List[TaggedToken]):
            return [token for token in tokens if token.word not in brackets]

        # This implementation wouldn't purge parentheses that cross parts (arg1, btwn, arg2) of a sentence.
        # Namely, when the matching half bracket is not found in the same "part", the parenthesis is not purged.
        # Once parentheses are purged, it would remove all dangling brackets
        self.befr = purge_brackets(purge_parenthsis(self.befr))
        self.ent1 = purge_brackets(purge_parenthsis(self.ent1))
        self.btwn = purge_brackets(purge_parenthsis(self.btwn))
        self.ent2 = purge_brackets(purge_parenthsis(self.ent2))
        self.aftr = purge_brackets(purge_parenthsis(self.aftr))

    def get_instance(self, crop=False, reverse=False, parens=False, num=False):
        self.insert_boundaries()
        if num:
            self.num_to_wildcard()
        if parens:
            self.purge_parentheses()
        if self.backward:
            if reverse:
                self.reverse()
            else:
                self.label = self.label + vocab.REVERED_SUFFIX
        self.assign_positions()
        tag_inserted = self.ent1 + self.btwn + self.ent2
        if not crop:
            tag_inserted = self.befr + tag_inserted + self.aftr
        return tag_inserted, self.label


if __name__ == '__main__':
    # for testing
    import sys
    from util.vocab import NONE_LABEL
    from util.readers import TextXmlReader, RelationsTxtReader

    t = TextXmlReader(sys.argv[1])
    if len(sys.argv) > 2:
        r = RelationsTxtReader(sys.argv[2], t.docs)
        for rel in r.relations:
            print(RelationInstance(rel.arg1, rel.arg2, rel.label, rel.backward).get_instance(
                crop=True, num=True, parens=True, reverse=True
            ))
    else:
        for doc in t.docs.values():
            for sent in doc.sents:
                print(sent.id, sent.ents)
                for e1, e2 in sent.entity_pairs():
                    r = RelationInstance(e1, e2, NONE_LABEL)
                    print(r.get_instance(crop=True, num=True, parens=True))
