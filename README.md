# Brandeis-REPROLANG2020-C.1

Brandeis Lab for Linguistics and Computation participles REPROLANG 2020 shared task session at LREC 2020, regarding *Task C.1 - Relation extraction and classification*. 
This repository holds the codebase from the participating team. `image/run.sh` contains the commands for all the experiments we did in this study.


## Setup

Requires python >= 3.7

0. (Optional but recommended) Create a conda env or virtual env
1. `pip install -r requirements.txt`
2. `python setup.py install`

## Arguments

`run.py` is our main portal where you can run various of experiments 
by giving different arguments


```
Usage: run.py -t SUBTASK [options]

Options:

-d  --download      Flag to download input datasets; train and test sets for classifier as well as train set for word embedding.
-w  --wetrain       Flag to train word embedding as described in the paper at runtime, based on ACLARC and arxiv.
-e  --wedim         Set dimension of word embedding vectors (to train a new embedding and/or to use in classification) (default: 200).
-t  --task          Choose which subtask to perform [1.1, 1,2, 2].
-p  --preprocess    Perform modules in the `preprocessors`. (default: ['crop', 'reverse', 'parens', 'num', 'merge'])
-n  --ensemble      Number of training rounds. At each round, both CNN classifier and RNN classifier will be trained with random initialization, and serialized into tensorflow ckpt files. These files will be used in the test time for ensemble averaging. (default 20)
-y  --synthesize    Flag to generate additional training data by synthesizing train and test data.
-o  --downsample    Optional float number to set downsample ratio. No downsampling when this option is not given.
-u  --upsample      Optional float number to set upsample ratio. No upsampling when this option is not given.
-b  --wetype        Choose which embeddings to use [w2v, fasttext]. default (w2v)
```

## Usage and examples

- This command line will download the dataset, and train 200-dimension word2vec embeddings first. 
Then it will perform subtask 1.1 with synthetic data at an ensemble size of 20.

    `run.py -dwye 200 -b w2v -t 1.1 -n 20`

- This command will perform subtask 2 with upsamping rate 1.0 using fasttext embeddings (assume we already have the dataset and pretrained embeddings). 

    `run.py -b fasttext -t 2 -u 1.0 -n 20`