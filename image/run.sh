# download dataset and train w2v embeddings for the first time
python3 $CODEBASE/run.py -we 200 -b w2v -t 1.1 -n 20  # subtask 1.1
python3 $CODEBASE/run.py -e 200 -b w2v -t 1.2 -n 20  # subtask 1.2
python3 $CODEBASE/run.py -e 200 -b w2v -t 2 -u 1.0 -n 20  # subtask 2


# train fasttext embeddings for the first time
python3 $CODEBASE/run.py -we 200 -b fasttext -t 1.1 -n 20  # subtask 1.1
python3 $CODEBASE/run.py -b fasttext -t 1.2 -n 20  # subtask 1.2
python3 $CODEBASE/run.py -b fasttext -t 2 -u 1.0 -n 20  # subtask 2
