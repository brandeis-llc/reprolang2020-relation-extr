#######################
# Choose an OS or runtime environment image that fits the needs of your experiment e.g.
# FROM debian:jessie
#Or:
FROM nvidia/cuda:10.0-cudnn7-runtime-ubuntu18.04
#######################

#Define input/output directories
VOLUME "/input"
VOLUME "/output/datasets"
VOLUME "/output/tables_and_plots"

#######################
# Customization start #
#######################

#Add any custom dependencies and/or scripts here
ARG codebase_dir=brandeis-reprolang2020-c1
ARG remote_codebase=https://gitlab.com/brandeis-llc/reprolang2020-relation-extr.git
ARG EXP_HOME=/experience

# system-level dependencies
ENV LC_ALL=C.UTF-8
# en_US is not available in the parent image
RUN apt-get update
RUN apt-get -y install python3 python3-pip python3-setuptools git build-essential libboost-all-dev cmake zlib1g-dev libbz2-dev liblzma-dev openjdk-8-jdk
RUN python3 -m pip install -U pip setuptools

# set up user and dirs
RUN useradd -mu27263 brandeis-llc
RUN mkdir -p /output /input /output/datasets /output/tables_and_plots $EXP_HOME
RUN chown -R brandeis-llc:brandeis-llc /output /input /output/datasets /output/tables_and_plots $EXP_HOME 
USER brandeis-llc
ENV CODEBASE=$EXP_HOME/$codebase_dir

RUN mkdir -p $CODEBASE
ADD --chown=brandeis-llc:brandeis-llc . $CODEBASE
WORKDIR $CODEBASE
RUN chmod u+x $CODEBASE/*.py
RUN python3 -m pip install --user -r $CODEBASE/requirements.txt
RUN python3 $CODEBASE/setup.py install --user

ENV INPUT=$CODEBASE/input
ENV OUTPUT=$CODEBASE/output
RUN ln -s /input $INPUT
RUN ln -s /output $OUTPUT

#######################
# Customization end   #
#######################

#Add and set entrypoint
ADD --chown=brandeis-llc:brandeis-llc run.sh /run.sh
RUN chmod u+x /run.sh
ENTRYPOINT /run.sh

