params = {
    "max_sent_len": list(range(7, 24)),
    "POS_embed_dim": list(range(10, 60, 10)),
    "position_embed_dim": list(range(10, 60, 10)),
    "batch_size": list(range(32, 193, 32)),
    "l2lambda": [0, 0.001, 0.01, 0.1, 1],
    "dropout": list(map(lambda x: x / 10, range(8))),
    "cnn_lr": [0.001, 0.005, 0.01, 0.05, 0.1],
    "rnn_lr": [0.001, 0.005, 0.01, 0.05, 0.1],
    "rnn_units": list(range(200, 2401, 300)),
    "cnn_filter_nums": list(range(64, 385, 64)),
    "cnn_filter_size": [list(range(2, 8))],
    "batches_per_step": [4000],
    "wetype": ['w2v', 'fasttext'],
    "wedim": list(range(100, 301, 100)),
    #  "downsample": list(map(lambda x: x / 100, range(0, 50, 5))),
    "upsample": list(map(lambda x: x / 100, range(0, 50, 5))),
    "ensemble": list(range(1,31))
}

param_keys = sorted(params.keys())
