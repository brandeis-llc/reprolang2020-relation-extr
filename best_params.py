params = {
    "max_sent_len": [19],
    "POS_embed_dim": [30],
    "position_embed_dim": [20],
    "batch_size": [64],
    "l2lambda": [0.01],
    "dropout": [0.5],
    "cnn_lr": [0.001],
    "rnn_lr": [0.005],
    "rnn_units": [600],
    "cnn_filter_nums": [192],
    "cnn_filter_size": [list(range(2, 8))],
    "batches_per_step": [4000],
    "wetype": ['w2v'],
    "wedim": [200],
    "downsample": [0.0],
    "upsample": [1.0],
    "ensemble": list(range(2, 3))
}

param_keys = sorted(params.keys())
