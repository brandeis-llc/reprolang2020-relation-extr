import re
from typing import List, Dict

from defusedxml import ElementTree as etree

from util.io_models import Entity, Relation, Sentence, Document
from util.nlp import CoreNLP, StanfordNLP, SpacyNLP
from util.vocab import NONE_LABEL
from util import path

TAGGER_CLS = {'corenlp': CoreNLP,
              'stanfordnlp': StanfordNLP,
              'spacy': SpacyNLP}


class TextXmlReader(object):
    def __init__(self, text_xml_filename, tagger='corenlp'):
        if "1.1." in text_xml_filename:
            self.dataset_id = 1
        elif "1.2." in text_xml_filename:
            self.dataset_id = 2
        else:
            self.dataset_id = 0
        # prep a corenlp server only for tokenization and sentence splitting
        self.tagger = TAGGER_CLS[tagger]()
        print(f"Reading {text_xml_filename}")
        self.docs = self.read_xmlfile(text_xml_filename)

    def read_sentences(self, node_etree: etree, sid_start: int) -> List[Sentence]:
        """
        Takes a <title> or <abstract> element from se2018t7 xml file and returns
        a list of `Sentence` objects.
        As this method does not know about the parent <text> element of <title>/<abstract>
        passed as input, the Sentence objects needs to be "inserted" to the `Document` obj
        that represents the parent <text> element after this method returns the list.

        `sid_start` param is required to properly assign sentence id's when processing
        <abstract> element, where there are existing sentences from <title> element
        from a previous call of this method.
        """
        if not node_etree:
            return []
        # joining with ' ' means force-tokenization at every opening and closing "<entity>" tag
        # this addresses following problems
        # 1. entity boundaries in the middle of tokens (e.g. in hyphenated word "<ent>Corpus-</ent>based")
        # 2. incorrect tokenization in the original data (e.g. below is from 1.2.text.xml)
        #   "<entity id="L08-1450.13">representation</entity> of ambiguous<entity id="L08-1450.14">data</entity> because they allow"
        # 3. helps corenlp ssplit with "... MT." sentences (although the one in '(1.2) C90-2046' remains unsplit even with this trick)
        text = " ".join(node_etree.itertext()).strip()

        # temporarily get all entity tags and their char offsets
        ent_nodes = []
        last_seen_char = 0 if node_etree.text is None else len(node_etree.text.strip())       # text from the beginning to the next open tag
        # findall for a XPATH to recursively find nested <entity>s
        for ent_node in node_etree.findall('.//entity'):
            ent_text = " ".join(ent_node.itertext())
            start = text[last_seen_char:].index(ent_text) + last_seen_char
            end = len(ent_text) + start
            last_seen_char = start
            if not ent_node.findall('entity'): # when no entities are nested in current node
                last_seen_char += len(ent_text)
                last_seen_char += len(ent_node.tail) if ent_node.tail else 0
            ent_nodes.append({'s': start, 'e': end, 'node': ent_node})

        # now get tokenization and sentence splitting using nlp application
        # putting the results in a list of Sentence objs
        sents = []
        tagged = self.tagger.tag(text)
        for sid, detected_sent in enumerate(tagged.sentence, sid_start):
            tokens = detected_sent.token

            sent = Sentence(sid, None,  # parent_doc will be set later
                            detected_sent.text,
                            [token.word for token in tokens],
                            [token.pos for token in tokens],
                            )
            sents.append(sent)

            # build maps from char offsets to token idx, using sapcy APIs
            tok_start_idx = dict([(token.beginChar, i) for i, token in enumerate(tokens)])
            tok_end_idx = dict([(token.endChar, i) for i, token in enumerate(tokens)])

            # finally create Entity objs and "insert" them to their parent sents
            while ent_nodes and ent_nodes[0]['s'] >= tokens[0].beginChar and ent_nodes[0]['e'] <= tokens[-1].endChar:


                # then, adjust entity start & end and look them up in the maps
                ent_node = ent_nodes.pop(0)
                sidx_key = ent_node['s']
                eidx_key = ent_node['e']
                # these while loops are to cope with trailing whitespaces in the in-line entity annotation
                # `if` conditionals in the while loops happens when an entity boundary is put in the middle of a token
                # (usually with hyphenated words "<ent>Corpus-</ent>based")
                ignore = False
                while not sidx_key in tok_start_idx:
                    sidx_key += 1
                    if sidx_key >= eidx_key:
                        ignore = True
                        break
                while not eidx_key in tok_end_idx:
                    eidx_key -= 1
                    if eidx_key <= sidx_key:
                        ignore = True
                        break
                if not ignore:
                    ent_stok_idx = tok_start_idx.get(sidx_key)
                    ent_etok_idx = tok_end_idx.get(eidx_key) + 1  # adding 1 because idx gotten from tok_end_idx is the index of the last token (the upper-bound of a slice should be exclusive)
                    Entity(int(ent_node['node'].get('id').split('.')[1]), sent, ent_stok_idx, ent_etok_idx)
                if ignore:
                    # print([token.word for token in tokens])
                    print("IGNORED", ent_node['node'].get('id'))
        return sents

    def dummy_relations(self):
        """
        Returns a dict of `Relation` objects without using actual labels, but only 'None' label,
        keyed with combined id's of its arguments.
        """
        relations = {}
        for doc in self.docs.values():
            for sent in doc.sents:
                for e1, e2 in sent.entity_pairs():
                    r = Relation(e1, e2, NONE_LABEL)
                    relations[r.get_id()] = r
        return relations

    def read_document(self, text_etree):
        """
        Takes a <text> node from se2018t7 xml files and return a `Document`,
        where sentences and entities are stored.
        """
        sents = []
        ttl = text_etree.find('title')
        sents.extend(self.read_sentences(ttl, len(sents)))
        abs = text_etree.find('abstract')
        sents.extend(self.read_sentences(abs, len(sents)))

        doc = Document(f"{self.dataset_id}-{text_etree.get('id')}")
        for sent in sents:
            doc.add_sent(sent)
        return doc

    def read_xmlfile(self, filename):
        xml = etree.parse(filename).getroot()
        docs = {}
        for text in xml.iter('text'):
            doc = self.read_document(text)
            docs[doc.id] = doc
        return docs


class RelationsTxtReader(object):

    def __init__(self, relations_txt_filename: str, docs: Dict[str, Document]):
        """
        `docs` is map from document_id to `Document` objs. It can be obtained using TextXmlReader instance.
        """
        if "1.1." in relations_txt_filename:
            self.dataset_id = 1
        elif "1.2." in relations_txt_filename:
            self.dataset_id = 2
        else:
            self.dataset_id = 0
        print(f"Reading {relations_txt_filename}")
        self.relations = self.read_realations_txt_file(relations_txt_filename, docs)

    def read_realations_txt_file(self, filename, docs):
        rels = []
        with open(filename) as in_f:
            for line in in_f:
                if not line.strip():
                    continue
                rels.append(self._get_relation(line, docs))
        return rels

    def _get_relation(self, text_line, docs: Dict[str, 'Document']) -> 'Relation':
        """
        read in each line of relation index like "USAGE(H01-1001.5,H01-1001.7,REVERSE)"
        and get the corresponding relations
        :param text_line:
        :return:
        """
        split_l = re.sub(r'[(),]', " ", text_line).split()
        rel_type = split_l[0]
        arg1_id = f"{self.dataset_id}-{split_l[1]}"
        arg2_id = f"{self.dataset_id}-{split_l[2]}"
        doc_id = arg1_id.split('.')[0]

        # quick sanity check
        doc2_id = arg2_id.split('.')[0]
        if doc_id != doc2_id:
            raise ValueError('Cross-document relations are now allowed!')

        arg1 = docs[doc_id].ents[arg1_id]
        arg2 = docs[doc_id].ents[arg2_id]
        return Relation(arg1, arg2, rel_type, len(split_l) == 4)


def get_training_data(dataset="1", task="1"):
    """
    Will return a list of `Relation` objects that can be converted to `RelationInstance` for training.

    dataset can be either "1.1", "1.2", "1".
    When dataset is "1", both 1.1 and 1.2 dataset will be used, otherwise only selected one will be used.

    task can be either "1.1", "1.2", "2".
    When task is "2", the returning list will contain 'None' relations that didn't appear in the X.relations.txt file.
    """

    if dataset == "1":
        xmlr1 = TextXmlReader(path.TRAIN_1_1_TXT_XML)
        xmlr2 = TextXmlReader(path.TRAIN_1_2_TXT_XML)
        if task == "2":
            xmlr1t = TextXmlReader(path.TEST_1_1_TXT_XML)
            xmlr2t = TextXmlReader(path.TEST_1_2_TXT_XML)
            rels = xmlr1.dummy_relations()
            rels.update(xmlr2.dummy_relations())
            for rel in RelationsTxtReader(path.TRAIN_1_1_REL_TXT, xmlr1.docs).relations:
                rels[rel.get_id()] = rel
            for rel in RelationsTxtReader(path.TRAIN_1_2_REL_TXT, xmlr2.docs).relations:
                rels[rel.get_id()] = rel
            for rel in RelationsTxtReader(path.PRED_TEST_1_1_REL_TXT, xmlr1t.docs).relations:
                rels[rel.get_id()] = rel
            for rel in RelationsTxtReader(path.PRED_TEST_1_2_REL_TXT, xmlr2t.docs).relations:
                rels[rel.get_id()] = rel
            rels = rels.values()
        else:
            rels = RelationsTxtReader(path.TRAIN_1_1_REL_TXT, xmlr1.docs).relations
            rels.extend(RelationsTxtReader(path.TRAIN_1_2_REL_TXT, xmlr2.docs).relations)

    else:
        if dataset == "1.1":
            xml = path.TRAIN_1_1_TXT_XML
            rel = path.TRAIN_1_1_REL_TXT
        else:
            xml = path.TRAIN_1_2_TXT_XML
            rel = path.TRAIN_1_2_REL_TXT
        xmlr = TextXmlReader(xml)
        if task == "2":
            rels = xmlr.dummy_relations()
            for rel in RelationsTxtReader(rel, xmlr.docs).relations:
                rels[rel.get_id()] = rel
        else:
            rels = RelationsTxtReader(rel, xmlr.docs).relations
    return rels


def get_test_data(task):
    if task == "2":
        xml = path.TEST_2_TXT_XML
        rel = path.TEST_2_REL_KEY
        xmlr = TextXmlReader(xml)
        rels = xmlr.dummy_relations()
        for rel in RelationsTxtReader(rel, xmlr.docs).relations:
            rels[rel.get_id()] = rel
        rels = rels.values()
    else:
        if task == "1.1":
            xml = path.TEST_1_1_TXT_XML
            rel = path.TEST_1_1_REL_KEY
        elif task == "1.2":
            xml = path.TEST_1_2_TXT_XML
            rel = path.TEST_1_2_REL_KEY
        xmlr = TextXmlReader(xml)
        rels = RelationsTxtReader(rel, xmlr.docs).relations

    return rels


if __name__ == '__main__':
    # for testing
    import sys
    t = TextXmlReader(sys.argv[1])
    for doc in t.docs.values():
        for sent in doc.sents:
            print(sent.id, sent.ents)
    if len(sys.argv) > 2:
        r = RelationsTxtReader(sys.argv[2], t.docs)
        for rel in r.relations:
            print(rel.label, rel.arg1.full_id(), rel.arg2.full_id())
