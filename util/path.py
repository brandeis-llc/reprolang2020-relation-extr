import sys
import os
from os.path import dirname, realpath
import datetime
from os.path import join as pjoin

PROJECT_ROOT = "."

### data sets

RESOURCES = pjoin(PROJECT_ROOT, '_input/')
if not os.path.exists(RESOURCES):
    os.mkdir(RESOURCES)
ACLARC_PATH = pjoin(RESOURCES, "aclarc")
if not os.path.exists(ACLARC_PATH):
    os.mkdir(ACLARC_PATH)
ARXIV_PATH = pjoin(RESOURCES, 'arxiv')
if not os.path.exists(ARXIV_PATH):
    os.mkdir(ARXIV_PATH)
TRAIN_PATH = pjoin(RESOURCES, 'se2018t7-train')
if not os.path.exists(TRAIN_PATH):
    os.mkdir(TRAIN_PATH)
TRAIN_1_1_REL_TXT = pjoin(TRAIN_PATH, '1.1.relations.txt')
TRAIN_1_1_TXT_XML = pjoin(TRAIN_PATH, '1.1.text.xml')
TRAIN_1_2_REL_TXT = pjoin(TRAIN_PATH, '1.2.relations.txt')
TRAIN_1_2_TXT_XML = pjoin(TRAIN_PATH, '1.2.text.xml')
TEST_PATH = pjoin(RESOURCES, 'se2018t7-test')
if not os.path.exists(TEST_PATH):
    os.mkdir(TEST_PATH)
TEST_1_1_REL_TXT = pjoin(TEST_PATH, '1.1.test.relations.txt')
PRED_TEST_1_1_REL_TXT = pjoin(TEST_PATH, '1.1.pred.test.relations.txt')
TEST_1_1_TXT_XML = pjoin(TEST_PATH, '1.1.test.text.xml')
TEST_1_2_REL_TXT = pjoin(TEST_PATH, '1.2.test.relations.txt')
PRED_TEST_1_2_REL_TXT = pjoin(TEST_PATH, '1.2.pred.test.relations.txt')
TEST_1_2_TXT_XML = pjoin(TEST_PATH, '1.2.test.text.xml')
TEST_2_TXT_XML = pjoin(TEST_PATH, '2.test.text.xml')
TEST_1_1_REL_KEY = pjoin(TEST_PATH, 'keys.test.1.1.txt')
TEST_1_2_REL_KEY = pjoin(TEST_PATH, 'keys.test.1.2.txt')
TEST_2_REL_KEY = pjoin(TEST_PATH, 'keys.test.2.txt')

ACLARC_CLEAN_DATA = pjoin(RESOURCES, 'aclarc-parscit-cleaned.txt')
ARXIV_DATA = pjoin(ARXIV_PATH, 'arxiv-cscl-2010-2017.txt')
TRAIN_CLEAN_DATA = pjoin(RESOURCES, 'train-clean.txt')

### outputs

OUTPUT = pjoin(PROJECT_ROOT, '_output/')
MODEL_OUTPUT = pjoin(OUTPUT, 'models')
def get_timestamp():
    return datetime.datetime.today().strftime('%Y%m%d-%H%M')
TABLE_OUTPUT = pjoin(OUTPUT, 'tables_and_plots')
if not os.path.exists(TABLE_OUTPUT):
    os.mkdir(TABLE_OUTPUT)


### NLP tools
# no support for windows
LMPLZ_SUFFIX = '-lin' if sys.platform == "linux" else '-mac'
LMPLZ_CMD = pjoin(PROJECT_ROOT, 'util', 'lmplz' + LMPLZ_SUFFIX)
LM_ARPA_PATH = pjoin(RESOURCES, 'kenlm.arpa')
LM_MMAP_PATH = pjoin(RESOURCES, 'kenlm.mmap')
CORENLP_PATH = pjoin(RESOURCES, 'stanford-corenlp-full-2018-10-05')
CORENLP_JAR = pjoin(CORENLP_PATH, 'stanford-corenlp-3.9.2.jar')
CORENLP_MODEL = pjoin(CORENLP_PATH, 'stanford-english-corenlp-2018-10-05-models.jar')

STANFORDNLP_MODELS_DOWNLOAD_PATH = pjoin(RESOURCES, 'stanfordnlp')
STANFORDNLP_MODELS_PATH = pjoin(STANFORDNLP_MODELS_DOWNLOAD_PATH, 'en_ewt_models')
STANFORDNLP_MODELS = [pjoin(STANFORDNLP_MODELS_PATH, 'en_ewt_lemmatizer.pt'),
                      pjoin(STANFORDNLP_MODELS_PATH, 'en_ewt_parser.pt'),
                      pjoin(STANFORDNLP_MODELS_PATH, 'en_ewt.pretrain.pt'),
                      pjoin(STANFORDNLP_MODELS_PATH, 'en_ewt_tagger.pt'),
                      pjoin(STANFORDNLP_MODELS_PATH, 'en_ewt_tokenizer.pt')]

### training configurations

CONFIGS = pjoin(PROJECT_ROOT, 'configs')
if not os.path.exists(CONFIGS):
    os.mkdir(CONFIGS)
PARAMS = pjoin(CONFIGS, 'params.json')


### (pre-)trained word embeddings
WE_PATH = pjoin(OUTPUT, 'word-embeddings')
if not os.path.exists(WE_PATH):
    os.mkdir(WE_PATH)
WE_100_FILE = pjoin(WE_PATH, 'w2v-100.model')
WE_200_FILE = pjoin(WE_PATH, 'w2v-200.model')
WE_300_FILE = pjoin(WE_PATH, 'w2v-300.model')
WE_FT_100_FILE = pjoin(WE_PATH, 'ft-100.model')
WE_FT_200_FILE = pjoin(WE_PATH, 'ft-200.model')
WE_FT_300_FILE = pjoin(WE_PATH, 'ft-300.model')

WE_FILE = {
    "w2v100": WE_100_FILE,
    "w2v200": WE_200_FILE,
    "w2v300": WE_300_FILE,
    "fasttext100": WE_FT_100_FILE,
    "fasttext200": WE_FT_200_FILE,
    "fasttext300": WE_FT_300_FILE,
}

