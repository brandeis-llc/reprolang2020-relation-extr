from typing import List, Dict


class Sentence(object):
    id: int
    doc: 'Document'
    text: str
    tokens: List[str]
    pos: List[str]
    ents: Dict[int, 'Entity']

    def __init__(self, sid, doc, text, tokens, pos=[]):
        self.id = sid
        self.text = text
        self.tokens = tokens
        self.ents = {}
        self.pos = pos
        self.doc = None
        if doc is not None:
            self.set_parent(doc)

    def __len__(self):
        return len(self.tokens)

    def __eq__(self, other):
        return self.id == other.id

    def __repr__(self):
        if self.pos:
            string = " ".join('/'.join(tok) for tok in zip(self.tokens, self.pos))
        else:
            string = " ".join(self.tokens)
        return f"<{self.doc.id if self.doc else 'NO'}++{self.id}: {string}"

    def set_parent(self, doc: 'Document'):
        doc.add_sent(self)
        self.doc = doc

    def add_entity(self, ent: 'Entity'):
        self.ents[ent.id] = ent
        if self.doc:
            self.doc.add_ent(ent)

    def text(self):
        return self.text

    def entity_pairs(self):
        nested = set()
        for e1 in self.ents.values():
            for e2 in self.ents.values():
                if e1 != e2 and not (e2.is_embedding(e1) or e1.is_embedding(e2)):
                    nested.add(tuple(sorted({e1, e2})))
        return nested


class Entity(object):

    def __init__(self, eid: int, sent: Sentence, stok_idx: int, etok_idx: int):
        self.id = eid
        self.sent = sent
        self.start = stok_idx
        self.end = etok_idx
        sent.add_entity(self)

    def full_id(self):
        return f'{self.sent.doc.id}.{self.id}'

    def is_embedding(self, other: 'Entity'):
        return self.start <= other.start and self.end >= other.end

    def __repr__(self):
        return f'({self.id}, {self.start}-{self.end}:{self.sent.tokens[self.start:self.end]})'

    def __gt__(self, other):
        return self.start > other.start


class Document(object):
    id: str
    sents: List['Sentence']

    def __init__(self, did: str):
        self.id = did
        self.sents = []
        self.ents = {}

    def add_sent(self, sent: 'Sentence'):
        self.sents.append(sent)
        sent.doc = self
        for ent in sent.ents.values():
            self.add_ent(ent)

    def add_ent(self, ent: 'Entity'):
        self.ents[f'{self.id}.{ent.id}'] = ent

    def text(self):
        return "\n".join([sent.text for sent in self.sents])


class Relation(object):
    arg1: Entity
    arg2: Entity
    sent: Sentence
    backward: bool
    label: str

    def __init__(self, arg1, arg2, label, backward=False):
        if arg1.sent != arg2.sent:
            raise ValueError(f"No suprasentential relations are allowed!: {arg1.full_id()}, {arg2.full_id()}, {label}")
        self.arg1 = arg1
        self.arg2 = arg2
        self.sent = arg1.sent
        self.label = label
        self.backward = backward

    def get_id(self):
        return "^".join(sorted((self.arg1.full_id(), self.arg2.full_id())))

    def arg1_words(self):
        return self._arg_text(self.arg1)

    def arg2_words(self):
        return self._arg_text(self.arg2)

    def _arg_words(self, arg):
        return self.sent.tokens[arg.start:arg.end]

    def arg1_postags(self):
        return self._arg_postags(self.arg1)

    def arg2_postags(self):
        return self._arg_postags(self.arg2)

    def _arg_postags(self, arg):
        return self.sent.pos[arg.start:arg.end]

    def postags_between(self):
        return self.sent.pos[self.arg1.end:self.arg2.start]

    def words_between(self):
        return self.sent.tokens[self.arg1.end:self.arg2.start]


