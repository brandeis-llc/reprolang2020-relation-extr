import copy
import random
from collections import Counter
from typing import List, Tuple

from preproc import RelationInstance, TaggedToken
from util import readers, nlp


def synthetic_data() -> List[RelationInstance]:
    print("=========")
    print("Data SYNTHESIS process")
    print("1) Preparing language model for filtering...")
    klm = nlp.Kenlm()
    btwn_srcs = readers.get_training_data("1.1", "1")
    # 1.1 + 1.2 + 2? 2 only?
    ents_srcs = readers.get_test_data("1.1")

    synth_instances = []
    print("2) Generating synthetic data")
    for ents_src in ents_srcs:
        for btwn_src in btwn_srcs:
            btwn = btwn_src.words_between()
            if len(btwn) > 4:
                ents_src_instance = RelationInstance(ents_src.arg1, ents_src.arg2, ents_src.label, False)
                synth_instance = RelationInstance(btwn_src.arg1, btwn_src.arg2, btwn_src.label, btwn_src.backward)
                synth_instance.ent1 = ents_src_instance.ent1
                synth_instance.ent2 = ents_src_instance.ent2
                synth_sent = " ".join([token.word for token in synth_instance.ent1 + synth_instance.btwn + synth_instance.ent2])
                synth_instances.append((synth_instance, klm.score(synth_sent)))
    synth_instances = [instance for instance, _ in sorted(synth_instances, key=lambda x: x[1], reverse=True)[:61]]
    # synth_instances = [instance for instance, prob in synth_instances if prob > -21]
    print(f'synthesized {len(synth_instances)} instances!')
    return synth_instances


def downsample(instances: List[Tuple[List[TaggedToken], str]], ratio=10.0):
    downsampled = []
    if ratio > 0:
        print(f"Downsampling training data (ratio: {ratio})")
        label_freq = Counter(label for instance, label in instances)
        most_freq_label, most_freq = label_freq.most_common(1)[0]
        most_freq = int(most_freq * ratio)
        other_freqs = sum(freq for _, freq in label_freq.most_common()[1:])
        label_dist = {}
        for instance in instances:
            label = instance[1]
            if label in label_dist:
                label_dist[label].append(instance)
            else:
                label_dist[label] = [instance]
        downsampled.extend(random.sample(label_dist[most_freq_label], int(other_freqs * ratio)))
        for label, insts in label_dist.items():
            if label != most_freq_label:
                downsampled.extend(insts)
        return downsampled
    else:
        return instances

def upsample(instances: List[Tuple[List[TaggedToken], str]], ratio=0.0):
    upsampled = []
    if ratio > 0:
        print(f"Upsampling training data (ratio: {ratio})")
        label_freq = Counter(label for instance, label in instances)
        most_freq_label, most_freq = label_freq.most_common(1)[0]
        most_freq = int(most_freq * ratio)
        other_freqs = sum(freq for _, freq in label_freq.most_common()[1:])
        label_dist = {}
        for instance in instances:
            label = instance[1]
            if label in label_dist:
                label_dist[label].append(instance)
            else:
                label_dist[label] = [instance]
        for label, insts in label_dist.items():
            upsampled.extend(insts)
            if label != most_freq_label:
                for i in range(1, most_freq // other_freqs):
                    upsampled.extend(copy.deepcopy(insts))
                fraction = int(most_freq % other_freqs * (label_freq[label] / other_freqs))
                upsampled.extend(copy.deepcopy(random.sample(insts, fraction)))
        return upsampled
    else:
        return instances


if __name__ == '__main__':
    news = synthetic_data()
    print(len(news))
    print(news[0].get_instance()[0])
