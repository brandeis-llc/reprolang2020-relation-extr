import datetime
import os
import subprocess
import time
import urllib
import zipfile

import kenlm
import spacy
from corenlp import CoreNLPClient
from embeddings import embeddings
from spacy.tokens import Doc

from util import path


class Kenlm():
    def __init__(self):
        if not (os.path.exists(path.LM_ARPA_PATH) and os.path.getsize(path.LM_ARPA_PATH) > 7000000000):
            with open(path.LM_ARPA_PATH, 'w') as arpa_file:
                self.lm_arpa_save(arpa_file)
        if os.path.exists(path.LM_MMAP_PATH):
            self.model = kenlm.Model(path.LM_MMAP_PATH)
        else:
            self.model = kenlm.Model(path.LM_ARPA_PATH)

    @staticmethod
    def lm_arpa_save(out_file):
        stime = time.time()
        print(f"Building .arpa file")
        # TODO (krim @ 11/30/19): we are not sure the order of language model used in the RHZ
        # as kenlm official documentation use 5-gram as the example, we'll keep that here
        training_data = "\n".join([open(training_file).read()
                                   for training_file in embeddings.prep_training_files()])
        subprocess.run([path.LMPLZ_CMD, '-o', '5'], universal_newlines=True, input=training_data, stdout=out_file, stderr=subprocess.DEVNULL)
        etime = time.time()
        print(f"ARPA file is created in {datetime.timedelta(seconds=round(etime-stime))}")

    def score(self, text):
        return self.model.score(text, bos=True, eos=True)


class NLP(object):
    pass


class CoreNLP(NLP):
    """CoreNLP is a wrapper for java-based Stanfrod CoreNLP"""
    def __init__(self):
        self.setup()
        os.environ['CORENLP_HOME'] = path.CORENLP_PATH
        self.tagger = self.tagger()

    def tag(self, text, **options):
        return self.tagger.annotate(text,
                                    annotators=['tokenize', 'ssplit', 'pos'],
                                    properties={
                                        # TODO (krim @ 11/26/19): maybe we can use `ptb3Escaping` option for tokenizer to suppress normalization (e.g. "(" --> "-LRB-" )
                                        'outputFormat': 'serialized',
                                        'tokenize.options': 'splitHyphenated=true',
                                        'ssplit.boundaryTokenRegex': r'[.]|[!?]+|[A-Z]{2,}[.]',
                                    },
                                    **options)

    @staticmethod
    def tagger():
        return CoreNLPClient(annotators=['tokenize', 'ssplit', 'pos'], timeout=60000, memory='8G')

    @staticmethod
    def tokenizer():
        return CoreNLPClient(annotators=['tokenize', 'ssplit'], timeout=60000, memory='8G')

    @staticmethod
    def setup():
        if not os.path.exists(path.CORENLP_JAR):
            print("CoreNLP is not found, downloading now...")
            if os.path.isdir(path.CORENLP_PATH):
                from shutil import rmtree
                rmtree(path.CORENLP_PATH)
            tmpf = urllib.request.urlretrieve('http://nlp.stanford.edu/software/stanford-corenlp-full-2018-10-05.zip', filename=None)[0]
            zipfile.ZipFile(tmpf, 'r').extractall(os.path.dirname(path.CORENLP_PATH))
        if not os.path.exists(path.CORENLP_MODEL):
            print('CoreNLP English model is not found, downloading now...')
            urllib.request.urlretrieve('http://nlp.stanford.edu/software/stanford-english-corenlp-2018-10-05-models.jar', filename=path.CORENLP_MODEL)
        print('CoreNLP is ready to go.')


class StanfordNLP(NLP):
    """StanfordNLP is a native python library based on neural models"""
    def __init__(self):
        self.download_en_models()
        self.nlp = stanfordnlp.Pipeline(processors='tokenize,mwt,pos')

    def tag(self):
        raise NotImplementedError

    @staticmethod
    def download_en_models():
        if not all(map(lambda x: os.path.exists(x), path.STANFORDNLP_MODELS)):
            stanfordnlp.download('en', path.STANFORDNLP_MODELS_DOWNLOAD_PATH)


class SpacyNLP(NLP):
    def __init__(self):
        self.nlp = spacy.load("en_core_web_sm", disable=["parser", "ner"])  # load in spacy parser
        self.nlp.tokenizer = self._dumb_tokenizer  # create customized tokenizer for spacy

    def tag(self, text):
        return self.nlp(text)

    def _dumb_tokenizer(self, text):
        """
        customized tokenizer for spacy
        :param text:
        :return:
        """
        tokens = []
        tokens.extend(text.split())
        return Doc(self.nlp.vocab, tokens)


if __name__ == '__main__':
    m = Kenlm()
    print(m.score("this is a sentences."))
