import numpy as np
import tensorflow as tf
from typing import Dict


def pred2file(test_file: str, pred: np.ndarray, label_mapping: Dict, out_file: str):
    label_mapping = {v: k for k, v in label_mapping.items()}
    y_pred = tf.argmax(pred, 1).numpy()
    new_lines = []
    with open(test_file, 'r') as in_f:
        for pred_label, line in zip(y_pred, in_f):
            if not line.strip():
                continue
            new_lines.append(label_mapping[int(pred_label)] + line)
    with open(out_file, 'w') as out_f:
        out_f.writelines(new_lines)
