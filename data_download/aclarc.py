import tarfile
import threading
import urllib.request
from os.path import dirname, realpath
from os.path import join as pjoin

from util import path


def get_aclarc_parscit_urls():
    with open(pjoin(dirname(realpath(__file__)), "aclarc-parscit.urls")) as url_file:
        return url_file.readlines()


def retrieve_and_extract_all(dest_path):
    threads = []
    urls = get_aclarc_parscit_urls()
    print(f"Downloading ACLARC (parscit) dataset to {dest_path}")
    for url in urls:
        url = url.strip()
        if len(url) > 0:
            thread = threading.Thread(target=extract_from_url, args=[url, dest_path])
            thread.daemon = True
            threads.append(thread)
            thread.start()

    for thread in threads:
        thread.join()
    print("")


def extract_from_url(tar_url, dest_path):
    tmpf = urllib.request.urlretrieve(tar_url, filename=None)[0]
    tarfile.open(tmpf, 'r:gz').extractall(dest_path)
    print(".",end="")


def download():
    retrieve_and_extract_all(path.ACLARC_PATH)


if __name__ == '__main__':
    import sys
    retrieve_and_extract_all(sys.argv[1])


