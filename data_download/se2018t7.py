import contextlib
import os
import urllib.request
from os.path import basename, dirname, realpath
from os.path import join as pjoin

from util import path


def download_train(dest_path):
    with open(pjoin(dirname(realpath(__file__)), "se2018t7train.urls")) as url_file:
        for url in url_file:
            filename = basename(url.strip())
            urllib.request.urlretrieve(url, filename=pjoin(dest_path, filename))


def download_test(dest_path):
    with open(pjoin(dirname(realpath(__file__)), "se2018t7test.urls")) as url_file:
        for url in url_file:
            filename = basename(url.strip())
            urllib.request.urlretrieve(url, filename=pjoin(dest_path, filename))


def download():
    train_path = path.TRAIN_PATH
    test_path = path.TEST_PATH
    with contextlib.suppress(FileExistsError):
        os.mkdir(train_path)
        os.mkdir(test_path)
    print(f"Downloading semeval2018-t7 train data to {train_path}")
    download_train(train_path)
    print(f"Downloading semeval2018-t7 test data to {test_path}")
    download_test(test_path)


