from . import aclarc, arxiv, se2018t7


def download_all():
    aclarc.download()
    arxiv.download()
    se2018t7.download()

