from typing import List, Iterable, Tuple, Dict

import numpy as np
from tensorflow.keras.preprocessing.sequence import pad_sequences

import util.vocab as voc
from preproc import TaggedToken


class RelationTransformer(object):
    def __init__(self, relations: List[Tuple[List[TaggedToken], str]], max_len: int, label_encoding: Dict[str, int], wrd_index=None):
        self.relations = relations
        if max_len is None:
            self.max_len = max(len(tokens) for tokens, label in relations)
        else:
            self.max_len = max_len
        print(f"LENGTH of longest sentence found: {self.max_len}")
        if wrd_index is None:
            self.wrd_index = self.index_words([tokens for tokens, label in relations])
        else:
            self.wrd_index = wrd_index
        self.tag_index = voc.POS_IDX
        self.pos_index = {pos: index for index, pos in enumerate(range(-self.max_len, self.max_len), 1)}
        self.label_index = label_encoding
        self.data, self.labels = self._compose_matrix()

    @staticmethod
    def index_words(relation_instances: List[List[TaggedToken]]):
        word_index = {v: i for i, v in enumerate(set(token.word for instance in relation_instances for token in instance))}
        word_index[voc.PAD_WRD] = len(word_index)
        word_index[voc.UNK_WRD] = len(word_index)
        # <e> (and ENT_END) should be already in the `vocab`
        return word_index

    def _token2idx(self, tokens: List[TaggedToken]):
        unk_idx = self.wrd_index[voc.UNK_WRD]
        token_idx = [self.wrd_index.get(t.word, unk_idx) for t in tokens]
        return token_idx

    def _tag2idx(self, tokens: List[TaggedToken]):
        unk_idx = self.tag_index[voc.UNK_POS]
        tag_idx = [self.tag_index.get(t.tag, unk_idx) for t in tokens]
        return tag_idx

    def _compose_matrix(self):
        token_list = []
        tag_list = []
        lr_position_list = []
        rl_position_list = []
        labels = []

        for relation in self.relations:
            labels.append(self.label_index[relation[1]])
            token_list.append(self._token2idx([token for token in relation[0]]))
            tag_list.append(self._tag2idx([token for token in relation[0]]))
            lr_position_list.append([self.pos_index.get(t.lpos, 0) for t in relation[0]])
            rl_position_list.append([self.pos_index.get(t.rpos, 0) for t in relation[0]])

        padded_tokens = self._padding(token_list)
        padded_tags = self._padding(tag_list)
        padded_lrp = self._padding(lr_position_list)
        padded_rlp = self._padding(rl_position_list)
        data_matrix = np.concatenate((padded_tokens, padded_tags, padded_lrp, padded_rlp), axis=1)
        labels = np.array(labels)
        return data_matrix, labels

    def _padding(self, idx_matrix: List[List]):
        padded = np.expand_dims(pad_sequences(idx_matrix, maxlen=self.max_len, padding='post', value=0), axis=1)
        return padded

