import tensorflow as tf
from tensorflow.keras import Model, layers, regularizers, initializers


class RelationBiLSTM(Model):
    def __init__(self, embeddings, tag_size, tag_dim, position_dim,
                 l2lambda, dropoutrate,
                 num_classes, max_sent_len, num_units=600):
        super(RelationBiLSTM, self).__init__()
        self.vocab_size, self.embeddings_dim = embeddings.shape

        self.embeddings = layers.Embedding(self.vocab_size, self.embeddings_dim, mask_zero=True)
        self.embeddings = layers.Embedding(self.vocab_size, self.embeddings_dim,
                                           embeddings_initializer=initializers.Constant(embeddings),
                                           mask_zero=True, trainable=True)
        self.tag_embeddings = layers.Embedding(tag_size, tag_dim, mask_zero=True, trainable=True)
        self.pos_embeddings = layers.Embedding(2 * max_sent_len + 1, position_dim, mask_zero=True, trainable=True)
        lstm_fw = layers.LSTM(units=num_units, kernel_regularizer=regularizers.l2(l2lambda))
        lstm_bw = layers.LSTM(units=num_units, kernel_regularizer=regularizers.l2(l2lambda), go_backwards=True)
        # BiRNN layer
        self.bi_lstm = layers.Bidirectional(lstm_fw, backward_layer=lstm_bw)
        # Output layer (num_classes).
        self.dropout = layers.Dropout(dropoutrate)
        self.out = layers.Dense(num_classes)

    def call(self, x, use_softmax=True):
        # A RNN Layer expects a 3-dim input (batch_size, seq_len, num_features).
        words = self.embeddings(x[:, 0])
        tags = self.tag_embeddings(x[:, 1])
        lpos = self.pos_embeddings(x[:, 2])
        rpos = self.pos_embeddings(x[:, 3])
        x = tf.concat([words, tags, lpos, rpos], axis=-1)
        # print(x._keras_mask)  # test if the mask is working
        x = self.bi_lstm(x)
        x = self.dropout(x)
        x = self.out(x)
        if use_softmax:
            x = tf.nn.softmax(x)
        return x


if __name__ == "__main__":
    pass
