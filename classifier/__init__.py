import json
import os
from typing import List, Tuple, Dict
from os.path import join as pjoin

import fasttext
import tabulate
import numpy as np
import tensorflow as tf
from gensim.models import Word2Vec

import preproc
from classifier.data_transform import RelationTransformer
from classifier.load_data import Dataset
from classifier.metrics import WeightedCrossEntropy, accuracy, F1, precision, recall, F1_labels, precision_labels, \
    recall_labels, F1_binary, precision_binary, recall_binary, F1_mi, precision_mi, recall_mi
from classifier.relation_CNN import RelationCNN
from classifier.relation_RNN import RelationBiLSTM
from util import path, vocab
from util.readers import RelationsTxtReader
from util.vocab import NONE_LABEL, POS_IDX


with open(path.PARAMS, 'r') as f:
    training_params = json.load(f)


def train(model, data: RelationTransformer, random_state, lr, out_name="relation_model"):
    dataset = Dataset(data.data, data.labels, batch_size=training_params['batch_size'])
    train_dataset, (X_val, y_val) = dataset.train_val_split(random_state=random_state)

    optimizer = tf.optimizers.Adam(lr, epsilon=1e-08)
    weighted_loss = WeightedCrossEntropy(data.labels, len(data.label_index))

    def run_optimization(x, y):
        # Wrap computation inside a GradientTape for automatic differentiation
        with tf.GradientTape() as g:
            # Forward pass
            pred = model(x, use_softmax=True)
            # Compute loss
            loss = weighted_loss(pred, y)
        # Variables to update
        trainable_variables = model.trainable_variables
        # Compute gradients
        gradients = g.gradient(loss, trainable_variables)
        # Update W and b following gradients
        optimizer.apply_gradients(zip(gradients, trainable_variables))

    highest_val_f1 = -1
    early_stop = 0
    steps = 0
    import time
    start_time = time.time()
    for step, (batch_x, batch_y) in enumerate(train_dataset.take(training_params['batches_per_step']), 1):
        # Run the optimization to update W and b values
        run_optimization(batch_x, batch_y)
        if step % 10 == 0:
            pred = model(batch_x, use_softmax=True)
            loss = weighted_loss(pred, batch_y)
            acc = accuracy(pred, batch_y)

            val_pred = model(X_val, use_softmax=True)
            val_f1 = F1(val_pred, y_val)
            val_pre = precision(val_pred, y_val)
            val_recall = recall(val_pred, y_val)
            print(
                f"step: {step:03} loss: {loss:.6f}\taccuracy: {acc:.4f}\tF1: {val_f1:.4f}\tP: {val_pre:.4f}\tR: {val_recall:.4f}")

            if val_f1 > highest_val_f1:
                highest_val_f1 = val_f1
                early_stop = 0
                model.save_weights(filepath=f"{out_name}.ckpt")
                steps = step
            else:
                early_stop += 1
            if early_stop > 10:
                break
    elapsed_secs = time.time() - start_time
    print("--- {} seconds elapsed ---".format(elapsed_secs))
    print("-----------------------------------")
    return steps, highest_val_f1, elapsed_secs


def index_word_vectors(we_dimention, vocab_index: Dict[str, int], embed_type) -> np.ndarray:
    embed_weights = []
    sorted_vocab_index = sorted(vocab_index.items(), key=lambda k: k[1])
    if embed_type == 'w2v':
        wv = Word2Vec.load(path.WE_FILE[embed_type + str(we_dimention)]).wv
        wv_vocab = wv.vocab
        for word, idx in sorted_vocab_index:
            if wv_vocab.get(word) is not None:
                embed_weights.append(wv[word])
            else:
                embed_weights.append(np.random.uniform(-1, 1, size=we_dimention))
    elif embed_type == 'fasttext':
        wv = fasttext.load_model(path.WE_FILE[embed_type + str(we_dimention)])
        for word, idx in sorted_vocab_index:
            if wv.get_word_vector(word) is not None:
                embed_weights.append(wv.get_word_vector(word))
            else:
                embed_weights.append(np.random.uniform(-1, 1, size=we_dimention))
    embed_weights = np.array(embed_weights)
    return embed_weights


def index_labels(training_instances: List[Tuple[List[preproc.TaggedToken], str]]) -> Dict[str, int]:
    from collections import Counter
    label_count = Counter(label for _, label in training_instances)
    print("==================")
    print("Labels found in the data:")
    print(label_count, f'\tTOTAL: {sum(label_count.values())}')
    print("==================")
    if len(label_count) == 6:
        return vocab.LABEL_INDICES[1]
    else:
        return vocab.LABEL_INDICES[2]


def vectorize(training_instances: List[Tuple[List[preproc.TaggedToken], str]], indexed_labels: Dict[str, int],
              max_len=None, wrd_index=None) -> RelationTransformer:
    return RelationTransformer(training_instances, max_len, label_encoding=indexed_labels, wrd_index=wrd_index)


def get_cnn_model(num_labels, max_sent_len, indexed_word_vectors) -> RelationCNN:
    print("Preparing CNN model")
    return RelationCNN(embeddings=indexed_word_vectors,
                       tag_size=len(POS_IDX),
                       tag_dim=training_params['POS_embed_dim'],
                       position_dim=training_params['position_embed_dim'],
                       filter_nums=training_params['cnn_filter_nums'],
                       filter_sizes=training_params['cnn_filter_size'],
                       l2lambda=training_params['l2lambda'],
                       dropoutrate=training_params['dropout'],
                       num_classes=num_labels,
                       max_sent_len=max_sent_len,
                       multi_channels=False)


def get_rnn_model(num_labels, max_sent_len, indexed_word_embedding) -> RelationBiLSTM:
    print("Preparing LSTM model")
    return RelationBiLSTM(embeddings=indexed_word_embedding,
                          tag_size=len(POS_IDX),
                          tag_dim=training_params['POS_embed_dim'],
                          position_dim=training_params['position_embed_dim'],
                          l2lambda=training_params['l2lambda'],
                          dropoutrate=training_params['dropout'],
                          num_classes=num_labels,
                          max_sent_len=max_sent_len,
                          num_units=training_params['rnn_units'])


def get_ensemble_weights(instances: List[List[preproc.TaggedToken]]) -> float:
    lengths = np.array([len(instance) for instance in instances]).reshape((-1, 1))
    max_len = np.max(lengths)
    min_len = np.min(lengths)
    s = lambda l: (l - min_len) / (max_len - min_len) - 0.5
    return 0.5 + np.multiply(np.sign(s(lengths)), np.power(s(lengths), 2))


def predict(model, test_data: RelationTransformer, ckpt_path=None) -> np.ndarray:
    if ckpt_path is not None:
        model.load_weights(ckpt_path)
    predicted = model(test_data.data)
    return predicted.numpy()


def averaged_prediction(model, test_data: RelationTransformer, ckpt_paths) -> np.ndarray:
    predictions = []
    for ckpt_path in ckpt_paths:
        predicted = predict(model, test_data, ckpt_path)
        predictions.append(predicted)
    return np.average(np.array(predictions), axis=0)


def ensemble_average(pred1: np.ndarray, pred2: np.ndarray, pred1_weight: float) -> np.ndarray:
    return pred1 * pred1_weight + pred2 * (1 - pred1_weight)


def evaluation(pred, gold, task_type: str, we_type: str):
    val_f1 = F1(pred, gold)
    val_pre = precision(pred, gold)
    val_recall = recall(pred, gold)
    # "macro average"

    val_f1_mi = F1_mi(pred, gold)
    val_pre_mi = precision_mi(pred, gold)
    val_recall_mi = recall_mi(pred, gold)
    # "micro average"

    val_f1_labels = F1_labels(pred, gold)
    val_pre_labels = precision_labels(pred, gold)
    val_recall_labels = recall_labels(pred, gold)
    # "per label"

    val_f1_bi = F1_binary(pred, gold)
    val_pre_bi = precision_binary(pred, gold)
    val_recall_bi = recall_binary(pred, gold)
    # "extraction only for subtask2"

    if task_type.startswith('1'):
        task1_overall_table([val_pre, val_recall, val_f1], task_type=task_type, we_type=we_type)
        task1_1_detailed_table(val_pre_labels, val_recall_labels, val_f1_labels,
                               [val_pre_mi, val_recall_mi, val_f1_mi], [val_pre, val_recall, val_f1], we_type, task_type)
    elif task_type == '2':
        task2_overall_table([val_pre, val_recall, val_f1], [val_pre_bi, val_recall_bi, val_f1_bi],
                            task_type=task_type, we_type=we_type)


def task1_1_detailed_table(p_labels, r_labels, f1_labels, micro_scores, macro_scores, we_type, task):
    title = f"The Precision (P), recall (R) and F1-score (F1) on the test set for each relation type " \
            f"for Subtask {task} using {we_type}"
    label_mapping = {
        'USAGE': 0,
        'MODEL-FEATURE': 1,
        'PART_WHOLE': 2,
        'TOPIC': 3,
        'RESULT': 4,
        'COMPARE': 5,
        'Micro-averaged': 6,
        'Macro-averaged': 7
    }
    p_labels = np.round(p_labels * 100, decimals=2).reshape((-1, 1))
    r_labels = np.round(r_labels * 100, decimals=2).reshape((-1, 1))
    f1_labels = np.round(f1_labels * 100, decimals=2).reshape((-1, 1))
    rel_types = np.array(list(label_mapping.keys())).reshape((-1, 1))
    micro = np.round(np.array(micro_scores) * 100, decimals=2).reshape((1, -1))
    macro = np.round(np.array(macro_scores) * 100, decimals=2).reshape((1, -1))

    table = np.concatenate((p_labels, r_labels, f1_labels), axis=1)
    table = np.concatenate((table, micro, macro), axis=0)
    table = np.concatenate((rel_types, table), axis=1)

    header = [f'Relation type ({we_type})', 'P', 'R', 'F1']
    table = tabulate.tabulate(table, headers=header, tablefmt='fancy_grid')
    with open(pjoin(path.TABLE_OUTPUT, f'subtask_1.1_detailed_result_{we_type}.txt'), 'w+') as f:
        f.write(title + '\n\n')
        f.write(table)
    print(title)
    print(table)


def task1_overall_table(macro_scores, task_type, we_type):
    title = f"The Precision (P), recall (R) and F1-score (F1) on the test set for Subtask {task_type} using {we_type}."
    macro = np.round(np.array(macro_scores) * 100, decimals=2).reshape((1, -1))
    table = np.concatenate((np.array([task_type]).reshape((1, 1)), macro), axis=1)
    header = [f'Subtask ({we_type})', 'P', 'R', 'F1']
    table = tabulate.tabulate(table, headers=header, tablefmt='fancy_grid')
    with open(pjoin(path.TABLE_OUTPUT, f'subtask_{task_type}_overall_result_{we_type}.txt'), 'w+') as f:
        f.write(title + '\n\n')
        f.write(table)
    print(title)
    print(table)


def task2_overall_table(c_macro_scores, e_macro_scores, task_type, we_type):
    title = f"The Precision (P), recall (R) and F1-score (F1) on the test set for Subtask {task_type} using {we_type}."
    c_macro = np.round(np.array(c_macro_scores) * 100, decimals=2).reshape((1, -1))
    e_macro = np.round(np.array(e_macro_scores) * 100, decimals=2).reshape((1, -1))
    macro = np.concatenate((e_macro, c_macro), axis=0)
    table = np.concatenate((np.array([f'{task_type}.E', f'{task_type}.C']).reshape((2, 1)), macro), axis=1)
    header = [f'Subtask ({we_type})', 'P', 'R', 'F1']
    table = tabulate.tabulate(table, headers=header, tablefmt='fancy_grid')
    with open(pjoin(path.TABLE_OUTPUT, f'subtask_{task_type}_overall_result_{we_type}.txt'), 'w+') as f:
        f.write(title + '\n\n')
        f.write(table)
    print(title)
    print(table)


if __name__ == "__main__":
    import sys
    import util

    instances = []
    t = util.readers.TextXmlReader(sys.argv[1])
    if len(sys.argv) > 2:
        r = RelationsTxtReader(sys.argv[2], t.docs)
        for rel in r.relations:
            instances.append(preproc.RelationInstance(rel.arg1, rel.arg2, rel.label).get_instance())
    else:
        for doc in t.docs.values():
            for sent in doc.sents:
                for e1, e2 in sent.entity_pairs():
                    instances.append(preproc.RelationInstance(e1, e2, NONE_LABEL).get_instance())
    indexed_labels = index_labels(instances)
    vectorized_data = vectorize(instances, indexed_labels)
    we = index_word_vectors(path.WE_FILE['w2v-200'], vectorized_data.wrd_index)
    cnn = get_cnn_model(len(indexed_labels), vectorized_data.max_len, we)
    train(model=cnn, data=vectorized_data, random_state=521,
          out_name=os.path.join(path.MODEL_OUTPUT, path.get_timestamp(), 'test-cnn'))
    rnn = get_rnn_model(len(indexed_labels), vectorized_data.max_len, we)
    train(model=rnn, data=vectorized_data, random_state=521,
          out_name=os.path.join(path.MODEL_OUTPUT, path.get_timestamp(), 'test-rnn'))
