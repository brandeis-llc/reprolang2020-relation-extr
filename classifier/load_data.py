"""
load train set only --> train/test split (cross validation) --> tf dataset
"""
import tensorflow as tf
from sklearn.model_selection import train_test_split, KFold


class Dataset(object):
    def __init__(self, data, labels, batch_size):
        self.data = data
        self.labels = labels
        self.batch_size = batch_size

    def _get_tf_dataset(self, data, labels):
        # is GPU OOM issue because of shuffling size? 
        shuffling_buffer = min(4000, len(data))
        dataset = tf.data.Dataset.from_tensor_slices((data, labels)).repeat().shuffle(buffer_size=len(data)//100)
        dataset = dataset.batch(batch_size=self.batch_size).prefetch(buffer_size=1)
        return dataset

    def train_val_split(self, val_size=0.2, shuffle=True, random_state=521):
        X_train, X_test, y_train, y_test = train_test_split(self.data, self.labels, test_size=val_size,
                                                            shuffle=shuffle, random_state=random_state)
        train_dataset = self._get_tf_dataset(X_train, y_train)
        return train_dataset, (X_test, y_test)

    def cross_validation(self, n_splits):
        kf = KFold(n_splits=n_splits, shuffle=True, random_state=521)
        for train_index, val_index in kf.split(self.data, self.labels):
            X_train, X_val = self.data[train_index], self.data[val_index]
            y_train, y_val = self.labels[train_index], self.labels[val_index]
            train_dataset = self._get_tf_dataset(X_train, y_train)
            yield train_dataset, (X_val, y_val)

    def __call__(self):
        return self._get_tf_dataset(self.data, self.labels)




