from collections import Counter
import numpy as np
from numpy import unique
import tensorflow as tf
from tensorflow.keras import backend, losses
from sklearn.metrics import f1_score, precision_score, recall_score


def cross_entropy_loss(y_pred, y_true):
    # Convert labels to int 64 for tf cross-entropy function.
    y_true = tf.cast(y_true, tf.int64)
    # Apply softmax to logits and compute cross-entropy.
    loss = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=y_true, logits=y_pred)
    # Average loss across the batch.
    return tf.reduce_mean(loss)


class WeightedCrossEntropy(losses.Loss):
    def __init__(self, labels, label_count):
        super(WeightedCrossEntropy, self).__init__()
        self.labels = labels
        self.label_count = label_count
        self.weights = self._compute_weight(labels)

    def _compute_weight(self, labels):
        label_type_count = sorted(Counter(labels).items())
        weights = [sum(Counter(labels).values()) / (self.label_count * c) for _, c in label_type_count]
        return tf.constant(weights, dtype=tf.float32)

    def call(self, y_pred, y_true):
        y_true = tf.one_hot(y_true, depth=self.label_count)
        # Clip prediction values to avoid log(0) error
        y_pred = tf.clip_by_value(y_pred, backend.epsilon(), 1 - backend.epsilon())
        return -tf.reduce_sum(y_true * tf.math.log(y_pred) * self.weights, 1)


def accuracy(y_pred, y_true):
    # Predicted class is the index of highest score in prediction vector (i.e. argmax).
    correct_prediction = tf.equal(tf.argmax(y_pred, 1), tf.cast(y_true, tf.int64))
    return tf.reduce_mean(tf.cast(correct_prediction, tf.float32), axis=-1)


def F1_binary(y_pred, y_true):
    y_pred = tf.argmax(y_pred, 1).numpy()
    last = np.max(y_true)
    y_pred = np.where(y_pred<last, 0, 1)
    y_true = np.where(y_true<last, 0, 1)
    return f1_score(y_true, y_pred, pos_label=0)


def precision_binary(y_pred, y_true):
    y_pred = tf.argmax(y_pred, 1).numpy()
    last = np.max(y_true)
    y_pred = np.where(y_pred<last, 0, 1)
    y_true = np.where(y_true<last, 0, 1)
    return precision_score(y_true, y_pred, pos_label=0)


def recall_binary(y_pred, y_true):
    y_pred = tf.argmax(y_pred, 1).numpy()
    last = np.max(y_true)
    y_pred = np.where(y_pred<last, 0, 1)
    y_true = np.where(y_true<last, 0, 1)
    return recall_score(y_true, y_pred, pos_label=0)


def F1_labels(y_pred, y_true):
    y_pred = tf.argmax(y_pred, 1).numpy()
    return f1_score(y_true, y_pred, average=None, labels=unique(y_true))


def precision_labels(y_pred, y_true):
    y_pred = tf.argmax(y_pred, 1).numpy()
    return precision_score(y_true, y_pred,   average=None, labels=unique(y_true))


def recall_labels(y_pred, y_true):
    y_pred = tf.argmax(y_pred, 1).numpy()
    return recall_score(y_true, y_pred, average=None,   labels=unique(y_true))

def F1_mi(y_pred, y_true):
    y_pred = tf.argmax(y_pred, 1).numpy()
    return round(f1_score(y_true, y_pred, average='micro', labels=unique(y_pred)), 4)


def precision_mi(y_pred, y_true):
    y_pred = tf.argmax(y_pred, 1).numpy()
    return round(precision_score(y_true, y_pred, average='micro', labels=unique(y_pred)), 4)


def recall_mi(y_pred, y_true):
    y_pred = tf.argmax(y_pred, 1).numpy()
    return round(recall_score(y_true, y_pred, average='micro', labels=unique(y_pred)), 4)

def F1(y_pred, y_true):
    y_pred = tf.argmax(y_pred, 1).numpy()
    return round(f1_score(y_true, y_pred, average='macro', labels=unique(y_pred)), 4)


def precision(y_pred, y_true):
    y_pred = tf.argmax(y_pred, 1).numpy()
    return round(precision_score(y_true, y_pred, average='macro', labels=unique(y_pred)), 4)


def recall(y_pred, y_true):
    y_pred = tf.argmax(y_pred, 1).numpy()
    return round(recall_score(y_true, y_pred, average='macro', labels=unique(y_pred)), 4)


if __name__ == "__main__":
    yt = tf.constant([0, 1, 2, 1])
    yp = tf.constant([
        [0.8, 0.1, 0.1],
        [0.1, 0.8, 0.1],
        [0.1, 0.1, 0.8],
        [0.1, 0.1, 0.8]])


