import tensorflow as tf
from tensorflow.keras import Model, layers, regularizers, initializers


class RelationCNN(Model):
    def __init__(self, embeddings, tag_size, tag_dim, position_dim,
                 filter_nums, filter_sizes, l2lambda, dropoutrate,
                 num_classes, max_sent_len, multi_channels=False):
        """

        :param embeddings: word embeddings
        :param filter_nums: number of filters
        :param filter_sizes: different sizes of filters
        :param num_classes: number of output classes
        :param max_sent_len: max sentence length
        """
        super(RelationCNN, self).__init__()
        self.multi_channels = multi_channels
        self.vocab_size, self.embeddings_dim = embeddings.shape

        # create the word embedding layer with pre-trained embedding weights, and make it trainable
        self.embeddings = layers.Embedding(self.vocab_size, self.embeddings_dim,
                                           embeddings_initializer=initializers.Constant(embeddings),
                                           trainable=True)
        if self.multi_channels:
            self.static_embeddings = layers.Embedding(self.vocab_size, self.embeddings_dim,
                                                      embeddings_initializer=initializers.Constant(embeddings),
                                                      trainable=False)
        # create POS and position embeddings with uniformly initialized weights
        self.tag_embeddings = layers.Embedding(tag_size, tag_dim, trainable=True)
        self.pos_embeddings = layers.Embedding(2 * max_sent_len + 1, position_dim, trainable=True)

        self.concurrent_cnn_layer = []
        self.concurrent_max_pool_layer = []
        self.filter_sizes = filter_sizes
        self.filter_nums = filter_nums
        for filter_size in self.filter_sizes:
            # create one conv and maxpooling layer to handle each one filter size
            self.concurrent_cnn_layer.append(layers.Conv2D(
                data_format="channels_last",
                filters=filter_nums,
                kernel_size=(filter_size, self.embeddings_dim + tag_dim + 2 * position_dim),
                strides=1,
                padding="valid",
                activation=tf.nn.relu,
                kernel_regularizer=regularizers.l2(l2lambda),
            ))
            self.concurrent_max_pool_layer.append(layers.MaxPool2D(
                pool_size=(max_sent_len - filter_size + 1, 1),
                strides=1,
                padding="valid"
            ))
        self.flatten = layers.Flatten()
        self.dropout = layers.Dropout(dropoutrate)
        self.out = layers.Dense(num_classes, kernel_regularizer=regularizers.l2(0.01))

    def call(self, x, use_softmax=True):
        features = []
        words = self.embeddings(x[:, 0])
        tags = self.tag_embeddings(x[:, 1])
        lpos = self.pos_embeddings(x[:, 2])
        rpos = self.pos_embeddings(x[:, 3])
        non_static_x = tf.concat([words, tags, lpos, rpos], axis=-1)  # concatenate all embeddings
        non_static_x = tf.expand_dims(non_static_x, -1)
        if self.multi_channels:
            static_words = self.static_embeddings(x[:, 0])
            static_x = tf.concat([static_words, tags, lpos, rpos], axis=-1)
            static_x = tf.expand_dims(static_x, -1)
            x = tf.concat([non_static_x, static_x], axis=-1)
        else:
            x = non_static_x

        for i in range(len(self.filter_sizes)):
            feature = self.concurrent_cnn_layer[i](x)
            feature = self.concurrent_max_pool_layer[i](feature)
            features.append(feature)
        flatted = self.flatten(tf.concat(features, axis=-1))
        flatted = self.dropout(flatted)
        out = self.out(flatted)
        if use_softmax:
            out = tf.nn.softmax(out)
        return out


if __name__ == "__main__":
    pass
