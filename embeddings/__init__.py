from . import embeddings
from util import path


def train(embed_type, dim):
    if embed_type == 'w2v':
        embeddings.word2vec_and_save(
                embeddings.prep_training_files(),
                path.WE_FILE[embed_type + str(dim)],
                dim
        )
    elif embed_type == 'fasttext':
        embeddings.fasttext_and_save(
            embeddings.prep_training_files(),
            path.WE_FILE[embed_type + str(dim)],
            dim
        )


