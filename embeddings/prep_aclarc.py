"""
Script to obtain clean text-only portion from the ACL-ARC corpus.
Currently only supports `ParsCit` version of the corpus. 
"""
import glob
import re
from os.path import join as pjoin

from bs4 import BeautifulSoup as soup

from util import path


def get_xmls(path):
    return glob.glob(pjoin(path, "**/*.xml"), recursive=True)


def get_used_algorithms(xml_path):
    xml = soup(open(xml_path), "xml")
    return [tag['name'] for tag in xml.find_all('algorithm')]


def get_parscit_body_text(xml_path):
    texts = []
    xml = soup(open(xml_path), "xml")
    doc = [alg for alg in xml.find_all("algorithm") if alg['name'] == "SectLabel"][0]
    for tag in doc.find_all(re.compile(r'.+')):
        if tag.name in ['title', 'listItem', 'bodyText', 'reference'] or tag.name.endswith('Header'):
            text = tag.string
            # because of kenlm restriction
            text = text.replace("<s>", r"<S>")
            text = text.replace("</s>", r"</S>")
            text = text.replace("<unk>", r"<UNK>")
            texts.append(text)
    return "".join(texts)


def get_all_tags(xml_path):
    """
    Function to peek existing tags and their text portion.
    This will serve for building prototypes of filters to filter non-body text. 

    An example from running this on one of `ParsCit` xml file. 

    ```
    title ['\nExtractin']
    note ['\nThomas C.']
    email ['\ntcr@nlm.n']
    sectionHeader ['\n3 Applica', '\nConclusio', '\nAcknowled']
    bodyText ['\nThe Speci', '\nThis erro', '\nFar more ']
    listItem ['\n(1) CC ch', '\n(10) Phos', '\n(8) A tra']
    figure ['\nBINDS\n<le', '\n<phosphat', '\n(12) Requ']
    page ['\n190\n', '\n193\n', '\n191\n']
    subsectionHeader ['\n1.2.2 Arg', '\n1.1 Ident', '\n1.2 Ident']
    subsubsectionHeader ['\n1.2.1 Arg']
    reference ['\nAit-Mokht']
    ```

    From this, I will consider 'title', '*Header', 'listItem', 'bodyText', 'reference' as sources of text of our interest

    """
    tags = {}
    xml = soup(open(xml_path), "xml")
    # in the parscit uses three different "algorithms" for different purposes (I guess)
    # and among 'SectLabel', 'ParsHed', 'ParsCit', "SectLabel" is the one that captures body text
    # So I'm only gonna use that part
    for alg in xml.find_all("algorithm"):
        if alg['name'] == "SectLabel":
            doc = alg
            break;
    for tag in doc.find_all(re.compile(r'.+')):
        texts = tags.get(tag.name, set())
        texts.add(tag.string[:10] if tag.string is not None else "")
        tags[tag.name] = texts
    return tags


def prep():
    with open(path.ACLARC_CLEAN_DATA, 'w') as text_file:
        for xml_file in get_xmls(path.ACLARC_PATH):
            text_file.write(get_parscit_body_text(xml_file))


if __name__ == "__main__":
    import argparse

    p = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description=__doc__
    )
    p.add_argument(
        '-x', '--xmls',
        required=True,
        action='store',
        nargs='?',
        help='directory name where ACL-ARC xml files are stored'
    )
    args = p.parse_args()
    #  for xml_path in get_xmls(args.xmls):
        #  print(get_parscit_body_text(xml_path))
    prep()
