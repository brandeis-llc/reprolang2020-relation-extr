import os
import re
import sys
import time
import datetime

import nltk.data
import nltk.tokenize
import fasttext
from gensim import models
from gensim import utils

from util import path
from . import prep_aclarc, prep_train, viz


class TrainingCorpus(object):
    """An interator that yields sentences (lists of str)."""

    def __init__(self, text_files):
        self.files = text_files

    def __iter__(self):
        for f in self.files:
            print("processing file {}".format(f))
            with open(f) as cur_file:
                content = cur_file.read()
                content = re.sub(r"\.", ". ", content)  # insert a space after all .'s
                content = re.sub(r"\s+", " ", content)  # remove duplicate whitespace
                sentences = nltk.tokenize.PunktSentenceTokenizer().tokenize(content)
                for sent in sentences:
                    sent = re.sub(r"[\n.,]", " ", sent)  # replace punctuation and new lines with spaces
                    sent = re.sub(r"\s+", " ", sent)     # remove duplicate whitespace
                    yield utils.simple_preprocess(sent)


def word2vec_and_save(text_files, model_path, dim):
    stime = time.time()
    print(f"Training word2vec embedding ({dim} dimentions)")
    corpus = TrainingCorpus(text_files)
    w2vmodel = models.Word2Vec(sentences=corpus, size=dim)
    etime = time.time()
    print(f"Training is done in {datetime.timedelta(seconds=round(etime-stime))}, saving model to {model_path}")
    w2vmodel.save(model_path)


def fasttext_and_save(text_files, model_path, dim):
    with open('ft_temp.txt', 'a') as temp_f:
        for f in text_files:
            print("processing file {}".format(f))
            with open(f) as cur_file:
                content = cur_file.read()
                content = re.sub(r"\.", ". ", content)  # insert a space after all .'s
                content = re.sub(r"\s+", " ", content)  # remove duplicate whitespace
                sentences = nltk.tokenize.PunktSentenceTokenizer().tokenize(content)
                for sent in sentences:
                    sent = re.sub(r"[\n.,]", " ", sent)  # replace punctuation and new lines with spaces
                    sent = re.sub(r"\s+", " ", sent)
                    temp_f.write(sent + '\n')

    stime = time.time()
    print(f"Training fasttext embedding ({dim} dimentions)")
    model_type = "cbow"
    dim = 200
    epoch = 5
    lr = 0.05
    model = fasttext.train_unsupervised(input='ft_temp.txt',
                                        model=model_type,
                                        dim=dim,
                                        lr=lr,
                                        epoch=epoch)

    etime = time.time()
    print(f"Training is done in {datetime.timedelta(seconds=round(etime-stime))}, saving model to {model_path}")
    model.save_model(model_path)


word2vec_training_data_size = {
    path.ACLARC_CLEAN_DATA: 553925817,
    path.TRAIN_CLEAN_DATA: 551749,
}


def data_exist(data_path):
    return os.path.exists(data_path) and os.path.getsize(data_path) == word2vec_training_data_size[data_path]


def prep_training_files():
    if not data_exist(path.TRAIN_CLEAN_DATA):
        print(f"Preparing a text file from training set for training a word2vec embedding. > \"{path.TRAIN_CLEAN_DATA}\"")
        prep_train.prep()
    else:
        print(f"Found cleaned training set text file at \"{path.TRAIN_CLEAN_DATA}\". Will use it for training a word2vec embedding.")
    if not data_exist(path.ACLARC_CLEAN_DATA):
        print(f"Preparing a text file from ACL-ARC corpus for training a word2vec embedding. > \"{path.ACLARC_CLEAN_DATA}\"")
        prep_aclarc.prep()
    else:
        print(f"Found cleaned ACL-ARC corpus text file at \"{path.ACLARC_CLEAN_DATA}\". Will use it for training a word2vec embedding.")
    return [path.ACLARC_CLEAN_DATA,
            path.TRAIN_CLEAN_DATA,
            path.ARXIV_DATA]


if __name__ == '__main__':
    corpus_path = sys.argv[1]
    model_path = sys.argv[2]
    word2vec_and_save([os.path.join(corpus_path, f) for f in os.listdir(corpus_path) if f.endswith(".txt")], model_path, dim=200)

    model = models.Word2Vec.load(model_path)
    x_vals, y_vals, labels = viz.reduce_dimensions(model)
    viz.plot_with_matplotlib(x_vals, y_vals, labels)
