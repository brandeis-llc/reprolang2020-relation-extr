"""
Script to get clean text version if se2018t7 training set.
The clean text will be used in training word embeddings.
"""
import glob
from os.path import join as pjoin

from bs4 import BeautifulSoup as soup

from util import path


def get_xmls(path):
    return glob.glob(pjoin(path, "**/*.xml"), recursive=True)


def get_text(xml_path):
    text = []
    for xmlfile in get_xmls(xml_path):
        xml = soup(open(xmlfile), "xml")
        text.append("".join(xml.findAll(text=True)))
    return "\n".join(text)


def prep():
    with open(path.TRAIN_CLEAN_DATA, 'w') as text_file:
        text_file.write(get_text(path.TRAIN_PATH))


