#! /usr/bin/env python3
"""
Pipeline script to run brandeis-llc-reprolang2020-c1 submission code. 
"""
import numpy
import itertools
import os
import sys
import random
from typing import List, Tuple

import classifier
from preproc import RelationInstance, TaggedToken
from util import path, readers, augmentation
from util.write_pred import pred2file

PIPELINE_ID = path.get_timestamp()


def download_all():
    import data_download
    data_download.download_all()


def train_word_embedding(dim, embed_type):
    import embeddings
    embeddings.train(embed_type, dim)


def prep_training_data(task, preprocessors, upsample_ratio, downsample_ratio, btwn_len, synthetic=False):
    dataset = "1" if 'merge' in preprocessors else task
    btwn_len_threshold = sys.maxsize
    if task == "2":
        try:
            preprocessors.remove('reverse')
        except ValueError:
            pass
        btwn_len_threshold = btwn_len
    training_instances = [RelationInstance(relation.arg1, relation.arg2, relation.label, relation.backward)
                          for relation in readers.get_training_data(dataset, task)]
    if synthetic:
        training_instances.extend(augmentation.synthetic_data())
    preprocessed_instances = [instance.get_instance(
        crop="crop" in preprocessors,
        reverse="reverse" in preprocessors,
        num="num" in preprocessors,
        parens="parens" in preprocessors,
    ) for instance in training_instances if instance.short_enough(btwn_len_threshold)]
    preprocessed_instances = augmentation.downsample(preprocessed_instances, downsample_ratio)
    preprocessed_instances = augmentation.upsample(preprocessed_instances, upsample_ratio)
    return preprocessed_instances


def prep_test_data(task, preprocessors):
    return [RelationInstance(relation.arg1, relation.arg2, relation.label, relation.backward).get_instance(
        crop="crop" in preprocessors,
        reverse="reverse" in preprocessors,
        num="num" in preprocessors,
        parens="parens" in preprocessors,
    ) for relation in readers.get_test_data(task)]


def train(instances, out_dirname, ensemble_size=20, wedim=200, embed_type='w2v'):
    print("TRAINING starts!")
    indexed_labels = classifier.index_labels(instances)
    vectorized_data = classifier.vectorize(instances, indexed_labels)
    training_data_we = classifier.index_word_vectors(wedim, vectorized_data.wrd_index, embed_type)
    sum_cnn_steps, sum_rnn_steps = 0, 0
    cnn_f1s = []
    rnn_f1s = []
    cnn_secs = []
    rnn_secs = []
    output_path = os.path.join(path.MODEL_OUTPUT, out_dirname)
    for cur_round in range(1, ensemble_size+1):
        random_state = random.randint(0, 10000)
        print(f"@ TRAINING ROUND: {cur_round:03}/{ensemble_size:03}, Random State: {random_state}")
        cnn = classifier.get_cnn_model(len(indexed_labels), vectorized_data.max_len, training_data_we)
        cnn_steps, cnn_f1, cnn_took = classifier.train(model=cnn, data=vectorized_data, random_state=random_state, lr=classifier.training_params['cnn_lr'], out_name=os.path.join(output_path, f'{cur_round:03}.cnn', 'weights'))
        rnn = classifier.get_rnn_model(len(indexed_labels), vectorized_data.max_len, training_data_we)
        rnn_steps, rnn_f1, rnn_took = classifier.train(model=rnn, data=vectorized_data, random_state=random_state, lr=classifier.training_params['rnn_lr'], out_name=os.path.join(output_path, f'{cur_round:03}.rnn', 'weights'))
        sum_cnn_steps += cnn_steps
        sum_rnn_steps += rnn_steps
        cnn_f1s.append(cnn_f1)
        rnn_f1s.append(rnn_f1)
        cnn_secs.append(cnn_took)
        rnn_secs.append(rnn_took)
    avg_cnn_steps = sum_cnn_steps // ensemble_size
    avg_rnn_steps = sum_rnn_steps // ensemble_size
    import statistics
    cnn_f1m = statistics.mean(cnn_f1s)
    cnn_f1v = statistics.pvariance(cnn_f1s, cnn_f1m)
    rnn_f1m = statistics.mean(rnn_f1s)
    rnn_f1v = statistics.pvariance(rnn_f1s, rnn_f1m)
    avg_f1m = statistics.mean((cnn_f1m, rnn_f1m))
    cnn_secsm = statistics.mean(cnn_secs)
    rnn_secsm = statistics.mean(rnn_secs)
    return vectorized_data.wrd_index, vectorized_data.max_len, training_data_we, \
           avg_cnn_steps, avg_rnn_steps, cnn_f1m, cnn_f1v, cnn_secsm, rnn_f1m, rnn_f1v, rnn_secsm, avg_f1m


def test(instances: List[Tuple[List[TaggedToken], str]], wrd_index, training_data_max_len, training_data_we, checkpoints_dir, task_type, we_type):
    print("TEST start!")
    indexed_labels = classifier.index_labels(instances)
    vectorized_data = classifier.vectorize(instances, indexed_labels, wrd_index=wrd_index, max_len=training_data_max_len)
    cnn = classifier.get_cnn_model(len(indexed_labels), training_data_max_len, training_data_we)
    rnn = classifier.get_rnn_model(len(indexed_labels), training_data_max_len, training_data_we)
    rnn_average = classifier.averaged_prediction(
        rnn, vectorized_data,
        get_checkpoints(checkpoints_dir, '.rnn')
    )

    cnn_average = classifier.averaged_prediction(
        cnn, vectorized_data,
        get_checkpoints(checkpoints_dir, '.cnn')
    )

    ensemble_weight = classifier.get_ensemble_weights([tokens for tokens, _ in instances])
    ensemble_props = classifier.ensemble_average(rnn_average, cnn_average, ensemble_weight)
    print("ensembled results:\n")
    classifier.evaluation(ensemble_props, vectorized_data.labels, task_type=task_type, we_type=we_type)
    if task_type == '1.1':
        pred2file(path.TEST_1_1_REL_TXT, ensemble_props, indexed_labels, path.PRED_TEST_1_1_REL_TXT)
    if task_type == '1.2':
        pred2file(path.TEST_1_2_REL_TXT, ensemble_props, indexed_labels, path.PRED_TEST_1_2_REL_TXT)


def get_checkpoints(checkpoints_dir, parent_suffix):
    output_path = os.path.join(path.MODEL_OUTPUT, checkpoints_dir)
    return [os.path.join(output_path, checkpoint_dir, 'weights.ckpt') for checkpoint_dir in
            [d for d in os.listdir(output_path) if d.endswith(parent_suffix)]]


def prep_parameter_grid(grid_fname, grid_recorder_fname):
    import importlib.util
    spec = importlib.util.spec_from_file_location('param_grid', grid_fname)
    gridmodule = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(gridmodule)
    with open(grid_recorder_fname, 'w') as grid_tsv:
        grid_tsv.write('\t'.join(gridmodule.param_keys + ['cnn_f1m', 'cnn_f1v', 'cnn_secsm', 'rnn_f1m', 'rnn_f1v', 'rnn_secsm', 'avg_f1m']))
        grid_tsv.write('\n')
    return gridmodule



def plot():
    pass


if __name__ == '__main__':
    import argparse
    print(sys.argv)
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description=__doc__
    )
    parser.add_argument(
        '-d', '--download',
        action='store_true',
        help='Flag to download input datasets; train and test sets for classifier as well as train set for word embedding.'
    )
    parser.add_argument(
        '-w', '--wetrain',
        action='store_true',
        help='Flag to train word embedding as described in the paper at runtime, based on ACLARC and arxiv, using gensim.models.Word2Vec .'
    )
    parser.add_argument(
        '-e', '--wedim',
        default=200,
        action='store',
        type=int,
        nargs='?',
        help='Set dimension of word embedding vectors (to train a new w2v and/or to use in classification) .'
    )
    parser.add_argument(
        '-t', '--task',
        required=True,
        action='store',
        choices=['1.1', '1.2', '2'],
        nargs='?',
        help=''
    )
    available_processors = ['crop', 'reverse', 'parens', 'num', 'merge']
    parser.add_argument(
        '-p', '--preprocess',
        default=available_processors,
        choices=available_processors,
        action='store',
        nargs='*',
        # by default, all modules in the `preprocessors` list will be applied.
        help=''
    )
    parser.add_argument(
        '-n', '--ensemble',
        default=20,
        type=int,
        action='store',
        nargs='?',
        help='Number of training rounds. At each round, both CNN classifier and RNN classifier will be trained with random initialization, and serialized into tensorflow ckpt files. These files will be used in the test time for ensemble averaging.'
    )
    parser.add_argument(
        '-y', '--synthesize',
        action='store_true',
        help='Flag to generate additional training data by synthesizing train and test data '
    )
    parser.add_argument(
        '-o', '--downsample',
        action='store',
        default=0.0,
        type=float,
        nargs='?',
        help='Option to set downsample ratio. No downsampling when this option is not given.'
    )
    parser.add_argument(
        '-u', '--upsample',
        action='store',
        default=0.0,
        type=float,
        nargs='?',
        help='Option to set upsample ratio. No upsampling when this option is not given.'
    )
    parser.add_argument(
        '--develop',
        action='store_true',
        help='secret option for developers'
    )

    parser.add_argument(
        '-b', '--wetype',
        action='store',
        default='w2v',
        choices=['w2v', 'fasttext'],
        nargs='?',
        help=''
    )

    parser.add_argument(
        '-g', '--grid',
        action='store',
        default='best_params.py',
        nargs='?',
        help=''
    )

    if len(sys.argv) == 1:
        parser.print_help(sys.stderr)
        sys.exit(1)
    args = parser.parse_args()
    if args.develop:
        print(args.upsample)
        exit()
    if args.download:
        download_all()
    PIPELINE_ID = PIPELINE_ID + f"-task{args.task}"
    grid_tsv_fname = os.path.join(path.TABLE_OUTPUT, PIPELINE_ID + '.grid.tsv')

    gridmodule = prep_parameter_grid(args.grid, grid_tsv_fname)
    print("++++++++++++++++++")
    print(len(list(itertools.product(*(gridmodule.params[key] for key in gridmodule.param_keys)))))
    print("grid search space")
    print("++++++++++++++++++")
    for gridnum, params in enumerate(itertools.product(*(gridmodule.params[key] for key in gridmodule.param_keys))):
        model_out_dirname = PIPELINE_ID + f"-grid{gridnum}"
        for i, key in enumerate(gridmodule.param_keys):
            classifier.training_params[key] = params[i]
        upsample = 0 if not args.task == "2" else classifier.training_params.get('upsample', args.upsample)
        downsample = classifier.training_params.get('downsample', args.downsample)
        wedim = classifier.training_params.get('wedim', args.wedim)
        wetype = classifier.training_params.get('wetype', args.wetype)
        ensemble = classifier.training_params.get('ensemble', args.ensemble)
        print("===================")
        print("CURRENT HYPERPARAMS")
        print(classifier.training_params)
        print("===================")
        if args.wetrain:
            train_word_embedding(dim=wedim, embed_type=wetype)

        trainset = prep_training_data(args.task, args.preprocess, upsample, downsample, btwn_len=classifier.training_params['max_sent_len'], synthetic=args.synthesize)
        word_index, max_len, embeddings, avg_cnn_steps, avg_rnn_steps, cnn_f1m, cnn_f1v, cnn_secsm, rnn_f1m, rnn_f1v, rnn_secsm, avg_f1m = \
            train(trainset, model_out_dirname, ensemble, wedim, wetype)
        with open(grid_tsv_fname, 'a') as grid_tsv:
            grid_tsv.write('\t'.join(map(str, list(params) + [avg_cnn_steps, avg_rnn_steps, cnn_f1m, cnn_f1v, cnn_secsm, rnn_f1m, rnn_f1v, rnn_secsm, avg_f1m])))
            grid_tsv.write('\n')
    grid_records_f = open(grid_tsv_fname)
    grid_records = grid_records_f.readlines()
    grid_records_f.close()
    f1s = [float(record.strip().split('\t')[-1]) for record in grid_records[1:]]
    best_params_idx = numpy.argmax(f1s)
    wetype_idx = gridmodule.param_keys.index('wetype')
    wetype = grid_records[best_params_idx+1].split('\t')[wetype_idx]
    print(f"\n{'+'*50}\nBEST GRID IDX: {best_params_idx}\nLOADING {PIPELINE_ID}-grid{best_params_idx} MODEL with {wetype} WORD EMBEDDING\n{'+'*50}")
    testset = prep_test_data(args.task, args.preprocess)
    test(testset, word_index, max_len, embeddings, PIPELINE_ID + f'-grid{best_params_idx}', args.task, wetype)

