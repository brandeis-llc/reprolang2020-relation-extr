import os
import subprocess

from setuptools import setup, find_packages

readme = open(os.path.join(os.path.dirname(__file__), "README.md")).read()
requirements = open(os.path.join(os.path.dirname(__file__), "requirements.txt")).read().splitlines()
try:
    num_gpus = len(subprocess.check_output(['nvidia-smi', '--query-gpu=gpu_name', '--format=csv']).decode().strip().split('\n'))
    tf = 'tensorflow-gpu' if num_gpus > 1 else 'tensorflow'
except FileNotFoundError:
    tf = 'tensorflow'

setup(
    name="brandeis-llc-reprolang2020-c1",
    version="0.0.2",
    python_requires='>=3.7',
    description="Brandeis-LLC submission for REPROLANG2020 Task C.1",
    license="MIT",
    keywords="reprolang",
    url="https://github.com/brandeis-llc/reprolang2020-relation-extr",
    packages=find_packages(),
    long_description=readme,
    classifiers=[
        "Development Status :: 3 - Alpha",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3.7"

    ],
    install_requires=[
        f'{tf}==2.0.0',
    ]
)
